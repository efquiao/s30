//Step 7 import express
const express = require("express");

//ODM
const mongoose = require("mongoose");

// Step 8 create express app
const app = express();

// Step 9, create a aconst called PORT
const port = 3001;

// Step 10 setup server to handle data from requests
//Allows our app to read JSON data, middleware
app.use(express.json());

// Allows our app to read data from forms
app.use(express.urlencoded({ extended: true }));

// Step11 Add listen method for requests
//

// Step12 Install Mongoose

// Step13 Immport Mongoose

// Step14 Go to Mongo DB Atlas and change the Network access to 0.0.0.0

// Step 15 Get connection string and password from mongodb

// Step 16, change data base to S30

// Step 17 connecting to MOngoDB Atlas - add .connet() metho
// mongoose.connect

// Step 18a Add the connection string as the 1st argument
// Step 18b Add this object to allow connection

// Step 19 Set Notification for conection succerss or failure by using .connection - //mongoose.connection

//mongodb+srv://admin:admin1234@zuitt-bootcamp.vhxdp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

//mongodb+srv://admin:admin1234@zuitt-bootcamp.vhxdp.mongodb.net/s30?retryWrites=true&w=majority

/*
{
  useNewUrlParser: true,
  useUnifiedToplogy; true
}
*/

mongoose.connect(
  "mongodb+srv://admin:admin1234@zuitt-bootcamp.vhxdp.mongodb.net/s30?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

//step20 store in a variable
let db = mongoose.connection;

//step21  print error for any error
db.on("error", console.error.bind(console, "connection error"));

//step22 connection successful confirmation

db.once("open", () => console.log("We're connected to the cloud database"));

//step23 schemas determine the structure of the document in the data base and act as a blue print-> new schema object
const taskSchema = new mongoose.Schema({
  //Define the fields with the corresponding data type
  //For a task, it needs a "task name" and "task status"
  //There is a field called "name" and its data type is "String"
  name: String,
  //There is a field called "status" that is a "string and the default value is pending"
  status: {
    type: String,
    default: "pending",
  },
});

//Models use Schemas and they act as the middleman from the server (JS code) to our database
//step24 - create a model
const Task = mongoose.model("Task", taskSchema);

/*
    Business Logic
    1. Add a functionality to check if there are duplicate tasks
        -If the task already exists in the database, return an error
        -If the task doesn't exist in the data base, add it to the database
 */

//Step25 Create route to add task
// app.post('/tasks', (req, res)=>{
//   req.body.name
// })

//Step 26 Check if the task already exist,
//Use the task model to interact with the task collection

//Task.findOne()
//Task.findOne({name:'Eat'})

//HTML Form - POSTMan

app.post("/tasks", (req, res) => {
  Task.findOne({ name: req.body.name }, (err, result) => {
    console.log(result);

    if (result != null && result.name == req.body.name) {
      return res.send("Duplicate task found");
    } else {
      let newTask = new Task({
        name: req.body.name,
      });
      newTask.save((saveErr, savedTask) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New task created");
        }
      });
    }
  });
});

//Step27 Get all tasks
// Get all the contents of the taks collection via the Task model
//"find", and an empty "{}" means it return all the documents and sotres them in the "result" pararmeter of the callback function

app.get("/tasks", (req, res) => {
  Task.find({}, (err, result) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({ data: result });
    }
  });
});

// Make a Schema
const userSchema = new mongoose.Schema({
  username: String,
  password: String,
  status: {
    type: String,
    default: "pending",
  },
});

// Make a Model
const User = mongoose.model("User", userSchema);

// Register a User

app.post("/signup", (req, res) => {
 
  User.findOne({ username: req.body.username }, (err, result) => {

  

    if (result != null && result.username == req.body.username) {
      return res.send("Duplicate name found");
    } else {

    //  if(req.body.username !== "" && req.body.password!===""){
    
      let newUser = new User({
        username: req.body.username,
        password: req.body.password
      });
      newUser.save((saveErr, savedTask) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New user created");
        }
      });
    }
  });
});

app.listen(port, () => console.log(`server running at port ${port}`));
